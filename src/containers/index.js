import React, { Component } from 'react';
import { Cropper } from 'react-image-cropper'
import '../styles/index.css';

class Index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            imgSrc: null,
            file: null,
            image: null,
            imageLoaded: false,
        }
    }

    componentDidMount() {
        const {JFCustomWidget} = global;
        const self = this;
        JFCustomWidget.subscribe("ready", function () {
            const buttonText = JFCustomWidget.getWidgetSetting('buttonName');
            const questionLabel = JFCustomWidget.getWidgetSetting('questionLabel');
            self.setState({buttonText, questionLabel})
            console.log({questionLabel, buttonText})
            const _this = self;
            //subscribe to form submit event
            JFCustomWidget.subscribe("submit", function () {
                const msg = {
                    //you should valid attribute to data for JotForm
                    //to be able to use youw widget as required
                    valid: true,
                    value: _this.state.image
                }
                // send value to JotForm
                JFCustomWidget.sendSubmit(msg);
            });
        });
    }

    handleImageLoaded() {
        this.setState({
            imageLoaded: true
        })
    }

    handleClick() {
        let node = this.image
        this.setState({
            image: node.crop()
        })
    }

    handleUpload = (e) => {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imgSrc: reader.result
            });
        }

        reader.readAsDataURL(file)
    }

    render() {
        return (
            <div className='container'>
                <h3>{this.state.questionLabel || 'Please upload a photo'}</h3>
                {!this.state.file ?
                    <div className='uploadPhoto'>
                        <input type='file' accept='image/*' onChange={this.handleUpload} />
                    </div> : null}
                {this.state.imageLoaded && !this.state.image ?
                    <div className='cropButtonContainer'>
                        <button onClick={() => this.handleClick()} className='cropButton' >
                            {this.state.buttonText || 'CROP'}
                        </button>
                    </div> : null}
                {this.state.image ?
                    <img
                        className="after-img"
                        src={this.state.image}
                        alt=""
                    />
                    :
                    <Cropper
                        src={this.state.imgSrc}
                        ref={ref => { this.image = ref }}
                        onImgLoad={() => this.handleImageLoaded()}
                        className='image'
                    />
                }
            </div>
        )
    }
}

export default Index;